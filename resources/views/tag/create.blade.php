@include('tag.header')
<title>@lang('tag.title')</title>
</head>
<body>
<div class="container contract">
    <form method="POST" action="{{route('tag.store')}}">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">@lang('tag.title.create')</h1>
        <div class="form-group">
            <label for="exampleFormControlInput1">@lang('tag.name.create')</label>
            <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Mr Jhon" required>
            @if($errors->has('name'))
                <p class="text-danger">{{$errors->first('name')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">@lang('tag.value.create')</label>
            <input type="text" name="value" class="form-control" id="exampleFormControlInput1" placeholder="14" required>
            @if($errors->has('value'))
                <p class="text-danger">{{$errors->first('value')}}</p>
            @endif
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">@lang('tag.button.create')</button>
        </div>

    </form>
</div>
</body>

@include('tag.footer')
