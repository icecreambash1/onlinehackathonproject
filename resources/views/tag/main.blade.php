@include('admin.head')
<link rel="stylesheet" href="{{asset('/styles/css/customAdmin.css')}}"/>
<title>@lang('admin.title_tags')</title>
<style>
    .blob {
        margin-top:20px;
    }
</style>
</head>
<body>
@include('admin.header', ['ru_lang'=>'/admin/tags','en_lang'=>'/en/admin/tags'])
@if(Session::has('tag_was_created'))
    <div class="alert alert-success alert-box" role="alert">
        {{Session::get('tag_was_created')}}
    </div>
@endif
@if(Session::has('tag_was_updated'))
    <div class="alert alert-success alert-box" role="alert">
        {{Session::get('tag_was_updated')}}
    </div>
@endif
@if(Session::has('tag_was_destroyed'))
    <div class="alert alert-danger alert-box" role="alert">
        {{Session::get('tag_was_destroyed')}}
    </div>
@endif


<div class="container blob">
    <h2>@lang('tag.tags_table_title')</h2>
    <h2><a href="{{route('tag.create')}}">@lang('tag.tags_create_title')</a></h2>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>@lang('tag.tags_table_name')</th>
            <th>@lang('tag.tags_table_value')</th>
            <th>@lang('tag.tags_table_edit_title')</th>
            <th>@lang('tag.tags_table_destroy_title')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)
        <tr>
            <td>{{$tag->id}}</td>
            <td>{{$tag->name}}</td>
            <td>{{$tag->value}}</td>
            <td>
                <a class="text-warning" href="{{route('tag.update',['id'=>$tag->id])}}">@lang('tag.tags_table_edit')</a>
            </td>
            <td>
                <a class="text-danger" href="{{route('tag.destroy',['id'=>$tag->id])}}">@lang('tag.tags_table_destroy')</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
