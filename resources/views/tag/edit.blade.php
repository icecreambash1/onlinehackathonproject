@include('tag.header')
@include('tag.header')
<title>@lang('tag.title.edit')</title>
<body>
<div class="container contract">
    <form method="POST" action="{{route('tag.update',$tag->id)}}">
        @csrf
        <h1 class="h3 mb-3 font-weight-normal">@lang('tag.title.edit.text') '{{$tag->name}}'</h1>
        <div class="form-group">
            <label for="exampleFormControlInput1">@lang('tag.name.create')</label>
            <input type="text"
                   name="name"
                   class="form-control"
                   id="exampleFormControlInput1"
                   placeholder="Mr Jhon"
                   value="{{$tag->name}}"
            >
            @if($errors->has('name'))
                <p class="text-danger">{{$errors->first('name')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">@lang('tag.value.create')</label>
            <input type="text"
                   name="value"
                   class="form-control"
                   id="exampleFormControlInput1"
                   placeholder="14"
                   value="{{$tag->value}}"
            >
            @if($errors->has('value'))
                <p class="text-danger">{{$errors->first('value')}}</p>
            @endif
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">@lang('tag.edit.button')</button>
        </div>
    </form>
</div>
</body>

@include('tag.footer')
