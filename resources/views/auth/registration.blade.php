<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/styles/css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="{{asset('/styles/css/customLogin.css')}}"/>

    <title>@lang('registration.reg.title')</title>
</head>
<body class="">
<div class="container">
    <form class="sign-in" method="POST" action="{{route('auth.registration.post')}}">
        @csrf
        <div class="priority">
            <img class="mb-4"
                 src="{{asset('/res/monkey.png')}}"
                 alt=""
                 width="72"
                 height="72"/>
            <h1 class="h3 mb-3 font-weight-normal">@lang('registration.reg.title.text')</h1>
        </div>
        <div class="sign-in-block">
            <label for="name">@lang('registration.reg.name.form')</label>
            <input type="text"
                   class="form-control"
                   placeholder="John Wick"
                   name="name"
                   required
                   value="{{old('name')}}"
            >
            @if($errors->has('name'))
                <p class="text-danger">{{$errors->first('name')}}</p>
            @endif
        </div>
        <div class="sign-in-block">
            <label for="email">@lang('registration.reg.email.form')</label>
            <input type="text"
                   class="form-control"
                   placeholder="example@monkeycode.com"
                   name="email"
                   required
                   value="{{old('email')}}"
            >
            @if($errors->has('email'))
                <p class="text-danger">{{$errors->first('email')}}</p>
            @endif
        </div>
        <div class="sign-in-block">
            <label for="password">@lang('registration.reg.password.form')
            </label>
            <input type="password"
                   class="form-control"
                   name="password"
                   placeholder="********"
                   required>
        </div>
        @if($errors->has('password'))
            <p class="text-danger">{{$errors->first('password')}}</p>
        @endif
        <div class="sign-in-button">
            <button class="btn btn-primary btn-block" type="submit">@lang('registration.reg.button.form')</button>
        </div>
        <a href="{{route('auth.login')}}">@lang('login.switch_form_login')</a>
        <p class="mt-5
                      mb-3
                      text-muted
                      priority">
            @if($app->getLocale() == 'ru')
                <a href="/en/auth/registration"> @lang('login.switch_lang_text') 🇺🇸 </a>
            @else
                <a href="/auth/registration"> @lang('login.switch_lang_text') 🇷🇺 </a>
            @endif
{{--            <a href="{{route('about.main')}}">@lang('login.about_squad_text')</a>--}}
            <br>
            © MonkeyCode Labs 2022
        </p>

    </form>
</div>
</body>
</html>
