<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/styles/css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="{{asset('/styles/css/customLogin.css')}}"/>

    <title>@lang('login.title')</title>
</head>
<body class="">
    <div class="container">
        <form class="sign-in" method="POST" action="{{route('auth.login')}}">
            @csrf
            <div class="priority">
            <img class="mb-4"
                 src="{{asset('/res/monkey.png')}}"
                 alt=""
                 width="72"
                 height="72"/>
            <h1 class="h3 mb-3 font-weight-normal">@lang('login.sign_in_title')</h1>
            </div>
                <div class="sign-in-block">
                    <label for="email">@lang('login.form_email')
                    </label>
                <input type="email"
                       class="form-control"
                       placeholder="example@monkeycode.com"
                       name="email"
                       value="{{$errors->first('old_email')}}"
                       required>
                    @if($errors->has('email'))
                        <p class="text-danger">{{$errors->first('email')}}</p>
                    @endif
                </div>
                <div class="sign-in-block">
                    <label for="password">@lang('login.form_password')

                    </label>
                    <input type="password"
                           class="form-control"
                           name="password"
                           placeholder="********"
                           required>
                    @if($errors->has('password'))
                        <p class="text-danger">{{$errors->first('password')}}</p>
                    @endif
                    @if($errors->has('msg'))
                        <p class="text-danger">{{$errors->first('msg')}}</p>
                    @endif
                </div>
                <div class="sign-in-button">
                <button class="btn btn-primary btn-block" type="submit">@lang('login.form_button')</button>
                </div>
                <a href="{{route('auth.registration')}}">@lang('login.switch_form_reg')</a>
                <p class="mt-5
                          mb-3
                          text-muted
                          priority">
                    @if($app->getLocale() == 'ru')
                        <a href="/en/auth/login"> @lang('login.switch_lang_text') 🇺🇸 </a>
                    @else
                        <a href="/auth/login"> @lang('login.switch_lang_text') 🇷🇺 </a>
                    @endif
{{--                    <a href="{{route('about.main')}}">@lang('login.about_squad_text')</a>--}}
                    <br>
                        © MonkeyCode Labs 2022
                </p>

        </form>
    </div>
</body>
</html>
