<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('/styles/css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="{{asset('/styles/css/customAbout.css')}}"/>
    <title>@lang('about.title')</title>
</head>
<body>
    <div class="cont">
        <div class="main-title">
            <img class="mb-4"
                 src="{{asset('/res/monkey.png')}}"
                 alt=""
                 width="72"
                 height="72"/>
            <h1 class="h3 mb-3 font-weight-normal">MonkeyCode Labs 2022.</h1>
            <p>@lang('about.title_text')</p>
        </div>
        <div class="justify-content-center">
        </div>
    </div>
</body>
</html>
