@include('admin.head')
<link rel="stylesheet" href="{{asset('/styles/css/customAdmin.css')}}"/>
<title>@lang('admin.title_dashboard')</title>
<script src="https://cdn.jsdelivr.net/combine/npm/chart.js@3.7.1,npm/chart.js@3.7.1/dist/chart.min.js,npm/chart.js@3.7.1/dist/chart.esm.min.js,npm/chart.js@3.7.1/dist/helpers.esm.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.esm.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/helpers.esm.min.js"></script>
<style>
    .conva {
        max-width:500px;
        max-height:500px;
    }
</style>
</head>

<body>
@include('admin.header', ['ru_lang'=>'/admin/dashboard','en_lang'=>'/en/admin/dashboard'])
<section class="h-100">
    <header class="container h-100">
        <div class="d-flex align-items-center justify-content-center h-100">
    <canvas class="conva" id="myChart"></canvas>
        </div>
    </header>
</section>




</body>
<script>
    const data = {
        labels: [
            @foreach($tags as $tag)
            '{{$tag->name}}',
            @endforeach
        ],
        datasets: [{
            label: 'My First Dataset',
            data: [
                @foreach($tags as $tag)
                {{$tag->value}},
                @endforeach
            ],
            backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                'rgb(255, 205, 86)'
            ],
            hoverOffset: 4
        }]
    };
    const config = {
        type: 'doughnut',
        data: data,
    };
</script>
<script>
    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
</script>
</html>
