<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="#">
        <img src="{{asset('/res/monkey-light.png')}}" width="30" height="30" class="d-inline-block align-top">
        @lang('admin.admin_panel')
    </a>

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item {{Route::is('dashboard') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('dashboard')}}">@lang('admin.title_dashboard')<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item {{Route::is('tag.index') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('tag.index')}}">@lang('admin.title_tags')</a>
        </li>

    </ul>
    <ul class="navbar-nav">

        <li class="nav-item">
            <a class="nav-link">{{\App\Helpers\Auth\AuthHelper::getUsername()}}({{\App\Helpers\Auth\AuthHelper::getEmail()}})</a>
        </li>
        <li class="nav-item">
            @if($app->getLocale() == 'ru')
                <a class="nav-link" href="{{URL::to('/')}}{{$en_lang}}"> @lang('login.switch_lang_text') 🇺🇸 </a>
            @else
                <a class="nav-link" href="{{URL::to('/')}}{{$ru_lang}}"> @lang('login.switch_lang_text') 🇷🇺 </a>
            @endif
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('auth.logout')}}">@lang('admin.logout')</a>
        </li>
    </ul>
</nav>
