@if($app->getLocale() == 'ru')
    <a href="/en/auth/registration"> @lang('login.switch_lang_text') 🇺🇸 </a>
@else
    <a href="/auth/registration"> @lang('login.switch_lang_text') 🇷🇺 </a>
@endif
