<?php

namespace App\Services\Locale;

interface LocalizationInterface
{
    public static function locale(): string;
}
