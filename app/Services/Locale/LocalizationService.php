<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Locale;

class LocalizationService implements LocalizationInterface
{

    public static function locale(): string
    {
        $locale = request()->segment(1, '');
        if (in_array($locale, config('app.locales'))) {
            return $locale;
        }
        return '';
    }
}
