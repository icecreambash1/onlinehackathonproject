<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Locale;

use Carbon\Laravel\ServiceProvider;

class LocalizationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(LocalizationInterface::class, LocalizationService::class);
    }
}
