<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\User;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(UserServiceInterface::class,UserService::class);
    }
}
