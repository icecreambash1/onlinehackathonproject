<?php

namespace App\Services\Auth;




interface AuthServiceInterface
{
    public function login($request,$flask);
    public function loginView();

    public function registration($flask);
    public function registrationView();

    public function logout($request);

}
