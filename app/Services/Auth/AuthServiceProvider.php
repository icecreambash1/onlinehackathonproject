<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Auth;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AuthServiceInterface::class,AuthService::class);
    }
}
