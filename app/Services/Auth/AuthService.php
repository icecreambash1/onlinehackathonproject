<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Auth;





use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class AuthService implements AuthServiceInterface
{

    public function login($request,$flask){

        $dude = User::where('email', '=', $flask['email'])->first();
        if(Hash::check($flask['password'], $dude->password)) {
            return $this->setSession($request,$dude);
        }
        return redirect()->route('auth.login')->withErrors([
            'msg'=>Lang::get('ccl.password_error'),
            'old_email'=>$flask['email']]);
    }

    public function loginView(){
        return view('auth.login');
    }

    public function registration($flask){
        return User::create([
            'name'=>$flask['name'],
            'email'=>$flask['email'],
            'password'=>Hash::make($flask['password']),
        ]);
    }

    public function registrationView(){
        return view('auth.registration');
    }

    private function setSession($request, $useragent) {
        $token = $useragent->session()->create([
            'token'=> Str::random(40)
        ]);
        $request->session()->put('UserAuth',$token->token);
        return redirect()->route('dashboard');
    }

    public function logout($request) {
        $request->session()->flush();
        return redirect()->route('auth.login');
    }
}
