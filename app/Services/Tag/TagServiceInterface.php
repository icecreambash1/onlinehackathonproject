<?php

namespace App\Services\Tag;

use App\Models\Tag;

interface TagServiceInterface
{
    public function list();
    public function create($flask);
    public function showCreate();
    public function showEdit($id);
    public function updateTag($flask);
    public function destroyTag($id);
}
