<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Tag;

use App\Models\Tag;

class TagService implements TagServiceInterface
{
    public function list()
    {
        return Tag::all();
    }

    public function create($flask)
    {
        return Tag::create($flask);
    }

    public function showCreate()
    {
        return view('tag.create');
    }

    public function showEdit($id)
    {
        $tag = Tag::find($id);
        return view('tag.edit')->with(['tag' => $tag]);
    }

    public function updateTag($flask)
    {
        $tag = Tag::find($flask['id']);
        $tag->update($flask);
        return $tag;
    }

    public function destroyTag($id)
    {
        $tag = Tag::destroy($id);
        return $tag;
    }
}
