<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Services\Tag;

use Illuminate\Support\ServiceProvider;

class TagServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(TagServiceInterface::class, TagService::class);
    }
}
