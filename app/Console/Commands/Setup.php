<?php

namespace App\Console\Commands;

use App\Models\Tag;
use App\Models\User;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = 'Mr Wick';
        $email = 'admin@admin.com';
        $password = 'adminadmin';
        User::create([
            'name'=>$name,
            'email'=>$email,
            'password'=>Hash::make($password)
        ]);

        for($i=0;$i<4;$i++) {
            Tag::create([
                'name'=>Factory::create()->name(),
                'value'=>Factory::create()->randomDigit()
            ]);
        }
        echo "User has been created: email($email), password($password)";
        return 1;
    }
}
