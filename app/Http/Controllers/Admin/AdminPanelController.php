<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Tag\TagService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminPanelController extends Controller
{
    public function dashboard(TagService $tag) {
        return view('admin.dashboard')->with(['tags'=>$tag->list()]);
    }
}
