<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tag\TagCreateRequest;
use App\Http\Requests\Tag\TagDestroyRequest;
use App\Http\Requests\Tag\TagEditRequest;
use App\Http\Requests\Tag\TagUpdateRequest;
use App\Models\Tag;
use App\Services\Tag\TagService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class TagController extends Controller
{
    public function index(TagService $tag)
    {
        return view('tag.main')->with(['tags' => $tag->list()]);
    }

    public function store(TagCreateRequest $request, TagService $tag)
    {
        $tag->create(flask: $request->only([
            'name',
            'value',
        ]));
        return redirect()->route('tag.index')->with([
            'tag_was_created' => Lang::get('tag.tag_was_created')]);
    }

    public function create(TagService $tag)
    {
        return $tag->showCreate();
    }

    public function edit(TagService $tag, $id, TagEditRequest $request)
    {
        $validated = $request->validated();

        return $tag->showEdit($id);
    }

    public function update($id, TagUpdateRequest $request, TagService $tag)
    {
        $validated = $request->validated();
        $bottle = $request->only([
            'name',
            'value',
            'id',
        ]);
        $tag->updateTag($bottle);

        return redirect()->route('tag.index')->with([
            'tag_was_updated' => Lang::get('tag.tag_was_updated')]);
    }

    public function destroy($id, TagDestroyRequest $request, TagService $tag)
    {
        $validated = $request->validated();

        $tag->destroyTag($id);

        return redirect()->route('tag.index')->with([
            'tag_was_destroyed' => Lang::get('tag.tag_was_destroyed')]);
    }
}
