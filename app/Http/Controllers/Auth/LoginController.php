<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Http\Controllers\Auth;

use App\Http\Requests\AuthLoginValidateRequest;
use App\Http\Requests\AuthRegistrationValidateRequest;
use App\Models\User;
use App\Services\Auth\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class LoginController
{
    public function loginShow(AuthService $user) {
        return $user->loginView();
    }

    public function login(AuthLoginValidateRequest $request, AuthService $user) {

        $validated = $request->validated();

        $bottle = $request->only([
            'email',
            'password',
        ]);

        return $user->login(flask:$bottle, request:$request);

    }

    public function registration(AuthRegistrationValidateRequest $request, AuthService $user) {

        $validated = $request->validated();

        $bottle = $request->only([
           'name',
           'email',
           'password',
        ]);

        $user->registration($bottle);

        return redirect(status:201)->route('auth.login');
    }

    public function registrationShow(AuthService $user) {
        return $user->registrationView();
    }

    public function logout(Request $request, AuthService $user) {
        return $user->logout($request);
    }
}
