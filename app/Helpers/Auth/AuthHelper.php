<?php
// Created by Icecream <icecream@jokyprod.com> 2022

namespace App\Helpers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthHelper
{
    public static function sayHello() {
        return 'Wassup';
    }

    public static function getUsername() {
        $session = Session::get('UserAuth');
        $auth = \App\Models\Session::where('token','=',$session)->first();
        return $auth->user()->first()->name;
    }
    public static function getEmail() {
        $session = Session::get('UserAuth');
        $auth = \App\Models\Session::where('token','=',$session)->first();
        return $auth->user()->first()->email;
    }
}
