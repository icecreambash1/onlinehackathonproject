<?php
// Created by Icecream <icecream@jokyprod.com> 2022
return [
    'title' => 'Войти в Админ-панель',
    'main_text'=>'Пожалуйста войдите',
    'sign_in_title'=>'Войдите',
    'form_email'=>'Почта',
    'form_password'=>'Пароль',
    'form_button'=>'Войти',
    'switch_lang_text' => 'Сменить язык',
    'about_squad_text' => 'О команде',
    'switch_form_reg' => 'Регистрация',
    'switch_form_login' => 'Авторизация',
];
