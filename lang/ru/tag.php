<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'title'=>'Тэги',
    'title.create' => 'Создать тег',
    'name.create' => 'Имя',
    'value.create' => 'Значение',
    'button.create' => 'Создать',
    'tag_was_created' => 'Тег был создан!',
    'tag_was_updated' => 'Тег был обновлен',
    'tag_was_destroyed' => 'Тег был удален!',
    'title.edit' => 'Редактировать тег',
    'title.edit.text' => 'Редактировать тег',
    'edit.button'=>'Применить',
    'tags_table_title'=>'Теги',
    'tags_table_name'=>'Имя',
    'tags_table_value'=>'Значение',
    'tags_table_edit_title'=>'Изменение',
    'tags_table_destroy_title'=>'Удаление',
    'tags_table_destroy'=>'Удалить',
    'tags_table_edit'=>'Изменить',
    'tags_create_title'=>'Создать тег'
];
