<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'reg.title' => 'Регистрация',
    'reg.title.text'=>'Регистрация',
    'reg.name.form'=>'Имя',
    'reg.email.form'=>'Почта',
    'reg.password.form'=>'Пароль',
    'reg.button.form'=>'Поехали',
];
