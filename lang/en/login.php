<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'title'=>'Login Admin',
    'main_text'=>'Login',
    'sign_in_title'=>'Please sign in',
    'form_email'=>'Email',
    'form_password'=>'Password',
    'form_button'=>'Sign in',
    'switch_lang_text' => 'Switch language',
    'about_squad_text' => 'About Squad',
    'switch_form_reg' => 'Registration',
    'switch_form_login' => 'Authorization',
];
