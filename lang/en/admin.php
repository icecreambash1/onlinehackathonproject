<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'admin_panel'=>'Admin Panel',
    'logout'=>'Logout',
    'title_dashboard'=>'Dashboard',
    'title_tags'=>'Tags'
];
