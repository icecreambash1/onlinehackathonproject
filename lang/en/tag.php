<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'title'=>'Tags',
    'title.create' => 'Create tag',
    'name.create' => "Name",
    'value.create' => 'Value',
    'button.create' => 'Create',
    'tag_was_created' => 'Tag was create!',
    'tag_was_updated' => 'Tag was update!',
    'tag_was_destroyed' => 'Tag was destroy!',
    'title.edit' => 'Edit Tag',
    'title.edit.text' => 'Edit tag',
    'edit.button'=>'Apply',
    'tags_table_title'=>'Tags',
    'tags_table_name'=>'Name',
    'tags_table_value'=>'Value',
    'tags_table_edit_title'=>'Edit',
    'tags_table_destroy_title'=>'Delete',
    'tags_table_destroy'=>'Delete',
    'tags_table_edit'=>'Edit',
    'tags_create_title'=>'Create tag'
];
