<?php
// Created by Icecream <icecream@jokyprod.com> 2022

return [
    'reg.title' => 'Registration',
    'reg.title.text'=>'Registration',
    'reg.name.form'=>'Name',
    'reg.email.form'=>'Email',
    'reg.password.form'=>'Password',
    'reg.button.form'=>'Boom',
];
