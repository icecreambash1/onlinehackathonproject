<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\AdminPanelController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/auth', 'middleware' => 'protected-was-auth-user'], function () {
    Route::get('/login', [LoginController::class, 'loginShow'])->name('auth.login');
    Route::post('/login', [LoginController::class, 'login'])->name('auth.login.post');
    Route::get('/registration', [LoginController::class, 'registrationShow'])->name('auth.registration');
    Route::post('/registration', [LoginController::class, 'registration'])->name('auth.registration.post');

});
Route::group(['prefix' => '/admin', 'middleware' => 'check-auth'], function () {
    Route::get('/dashboard', [AdminPanelController::class, 'dashboard'])->name('dashboard');
    Route::group(['prefix' => '/tags'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\TagController::class, 'index'])->name('tag.index');
        Route::post('/', [\App\Http\Controllers\Admin\TagController::class, 'store'])->name('tag.store');
        Route::get('/create', [\App\Http\Controllers\Admin\TagController::class, 'create'])->name('tag.create');
        Route::get('/{id}/edit', [\App\Http\Controllers\Admin\TagController::class, 'edit'])->name('tag.edit');
        Route::post('/{id}/edit', [\App\Http\Controllers\Admin\TagController::class, 'update'])->name('tag.update');
        Route::get('/{id}/delete', [\App\Http\Controllers\Admin\TagController::class, 'destroy'])->name('tag.destroy');

    });
});
Route::group(['prefix' => '/user'], function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('auth.logout');
});
Route::get('/', function () {
    return redirect()->route('dashboard');
});

